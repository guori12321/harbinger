* 注册bitbucket.org的账号

* 添加bitbucket的ssh key

参考 https://confluence.atlassian.com/pages/viewpage.action?pageId=270827678

* fork本项目 https://bitbucket.org/guori12321/harbinger

* 安装 git

`sudo apt-get install git`

* clone你fork后的项目

把下面的`Your_Name`换成你的名字

```
git clone ssh://git@bitbucket.org/Your_Name/harbinger.git
```

* 对你本地的git库进行配置

首先进入你的clone后的库的地址，再执行下面的操作。

在你的库里，新建一个文件，文件名是你的姓名的拼音，文件的内容是你的名字的汉字

之后是把你更改的内容推送到你的bitbucket上。进行如下操作：

注意把**guori12321**换成你的id。

```
git remote add origin ssh://git@bitbucket.org/guori12321/harbinger.git
```

```
git push -u origin --all # pushes up the repo and its refs for the first time
```

把`看日记学git`的前10天的部分看一下，操作一遍

## 关于python
把`python简明教程`看几遍



